//
//  GameView.h
//  Doodle2
//
//  Created by Patrick Madden on 2/4/17.
//  Copyright © 2017 Binghamton CSD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Jumper.h"
#import "IceBlock.h"
#import "LavaBlock.h"
#import "CrackedBlock.h"

@interface GameView : UIView {

}
@property (nonatomic, strong) Jumper *jumper;
@property (nonatomic) NSMutableArray *iceBlockArr;
@property (nonatomic) NSMutableArray *lavaBlockArr;
@property (nonatomic) NSMutableArray *crackedBlockArr;
@property (nonatomic) float tilt;
-(void)arrange:(CADisplayLink *)sender;

@end
