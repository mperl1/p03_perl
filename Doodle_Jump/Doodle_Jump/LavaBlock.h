//
//  LavaBlock.h
//  Doodle_Jump
//
//  Created by Matt Perl on 2/25/17.
//  Copyright © 2017 Binghamton CSD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LavaBlock : UIImageView

@end
