//
//  GameView.m
//  Doodle2
//
//  Created by Patrick Madden on 2/4/17.
//  Copyright © 2017 Binghamton CSD. All rights reserved.
//

#import "GameView.h"

@implementation GameView
@synthesize jumper;
@synthesize iceBlockArr;
@synthesize lavaBlockArr;
@synthesize crackedBlockArr;
@synthesize tilt;

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"lamebackground.jpg"]];
        CGRect bounds = [self bounds];
        
        UIImage *pengImg = [UIImage imageNamed:@"eggbert.png"];
        jumper = [[Jumper alloc] initWithFrame:CGRectMake(bounds.size.width/2, bounds.size.height - 20, 35, 35)];
        [jumper setImage:pengImg];
        [jumper setDx:0];
        [jumper setDy:10];
        [self addSubview:jumper];
        [self makeBricks:nil];
    }
    return self;
}

-(IBAction)makeBricks:(id)sender
{
    CGRect bounds = [self bounds];
    float width = bounds.size.width * .2;
    float height = 20;
    
    /*
    if (bricks)
    {
        for (Brick *brick in bricks)
        {
            [brick removeFromSuperview];
        }
    }
    */
    
    if(iceBlockArr){
        for(IceBlock *ib in iceBlockArr){
            [ib removeFromSuperview];
        }
    }
    
    if(lavaBlockArr){
        for(LavaBlock *lb in lavaBlockArr){
            [lb removeFromSuperview];
        }
    }
    
    if(crackedBlockArr){
        for(CrackedBlock *cb in crackedBlockArr){
            [cb removeFromSuperview];
        }
    }
        
    //bricks = [[NSMutableArray alloc] init];
    iceBlockArr = [[NSMutableArray alloc] init];
    lavaBlockArr = [[NSMutableArray alloc] init];
    crackedBlockArr = [[NSMutableArray alloc] init];
    for (int i = 0; i < 10; ++i)
        {
            int blockType = [self generateBlockType];
            
            if(blockType == 0){
                IceBlock *b = [[IceBlock alloc] initWithFrame:CGRectMake(0, 0, width, height)];
                [b setImage:[UIImage imageNamed:@"iceblock.png"]];
                [b setCenter:CGPointMake(rand() % (int)(bounds.size.width * .8), rand() % (int)(bounds.size.height * .8))];
                [iceBlockArr addObject:b];
                [self addSubview:b];
            }
            else if(blockType == 1){
                LavaBlock *b = [[LavaBlock alloc] initWithFrame:CGRectMake(0, 0, width, height)];
                [b setImage:[UIImage imageNamed:@"lavablock.png"]];
                [b setCenter:CGPointMake(rand() % (int)(bounds.size.width * .8), rand() % (int)(bounds.size.height * .8))];
                [lavaBlockArr addObject:b];
                [self addSubview:b];
            }
            else{
                CrackedBlock *b = [[CrackedBlock alloc] initWithFrame:CGRectMake(0, 0, width, height)];
                [b setImage:[UIImage imageNamed:@"crackedblock.png"]];
                [b setCenter:CGPointMake(rand() % (int)(bounds.size.width * .8), rand() % (int)(bounds.size.height * .8))];
                [crackedBlockArr addObject:b];
                [self addSubview:b];
            }
            /*
            Brick *b = [[Brick alloc] initWithFrame:CGRectMake(0, 0, width, height)];
            [b setBackgroundColor:[UIColor blueColor]];
            [self addSubview:b];
            [b setCenter:CGPointMake(rand() % (int)(bounds.size.width * .8), rand() % (int)(bounds.size.height * .8))];
            [bricks addObject:b];
             */
        }
}

-(int)generateBlockType{
    return (arc4random()%3);
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void) shiftBlocksDown{
    CGRect bounds = [self bounds];
    CGPoint p;
    int randX;
    int randY;
    int xSpacing = 0;
    for(int i = 0; i < [iceBlockArr count]; i++){
        IceBlock *tmp = [iceBlockArr objectAtIndex:i];
        p = tmp.frame.origin;
        
        if((p.y+jumper.dy) >= bounds.size.height){
            randX = arc4random_uniform((int)(bounds.size.width*.8))+xSpacing;
            randY = arc4random_uniform((int)(bounds.size.height*.1));
            [[iceBlockArr objectAtIndex:i] setFrame: CGRectMake(randX, randY, tmp.frame.size.width, tmp.frame.size.height)];
        }
        else{
            [[iceBlockArr objectAtIndex:i] setFrame:CGRectMake(p.x, (p.y+jumper.dy), tmp.frame.size.width, tmp.frame.size.height)];
        }
        xSpacing += 2;
    }
    
    for(int i = 0; i < [lavaBlockArr count]; i++){
        LavaBlock *tmp = [lavaBlockArr objectAtIndex:i];
        p = tmp.frame.origin;
        
        if((p.y+jumper.dy) >= bounds.size.height){
            randX = arc4random_uniform((int)(bounds.size.width*.8))+xSpacing;
            randY = arc4random_uniform((int)(bounds.size.height*.1))+2;
            [[lavaBlockArr objectAtIndex:i] setFrame: CGRectMake(randX, randY, tmp.frame.size.width, tmp.frame.size.height)];
        }
        else{
            [[lavaBlockArr objectAtIndex:i] setFrame:CGRectMake(p.x, (p.y+jumper.dy), tmp.frame.size.width, tmp.frame.size.height)];
        }
        xSpacing += 2;
    }
    
    for(int i = 0; i < [crackedBlockArr count]; i++){
        CrackedBlock *tmp = [crackedBlockArr objectAtIndex:i];
        p = tmp.frame.origin;
        
        if((p.y+jumper.dy) >= bounds.size.height){
            randX = arc4random_uniform((int)(bounds.size.width*.8))+xSpacing;
            randY = arc4random_uniform((int)(bounds.size.height*.1))+4;
            [[crackedBlockArr objectAtIndex:i] setFrame: CGRectMake(randX, randY, tmp.frame.size.width, tmp.frame.size.height)];
        }
        else{
            [[crackedBlockArr objectAtIndex:i] setFrame:CGRectMake(p.x, (p.y+jumper.dy), tmp.frame.size.width, tmp.frame.size.height)];
        }
        xSpacing += 2;
    }
}


-(void)arrange:(CADisplayLink *)sender
{
    //CFTimeInterval ts = [sender timestamp];
    
    CGRect bounds = [self bounds];
    
    // Apply gravity to the acceleration of the jumper
    [jumper setDy:[jumper dy] - .3];
    
    // Apply the tilt.  Limit maximum tilt to + or - 7
    //Note: Tilt management taken from Ryan Simpson
    
    int target = tilt*((bounds.size.width/2 - 20)+bounds.size.width/2);
    [jumper setDx:(target - [jumper center].x)/10];
    if ([jumper dx] > 7)
        [jumper setDx:7];
    if ([jumper dx] < -7)
        [jumper setDx:-7];
    
    // Jumper moves in the direction of gravity
    CGPoint p = [jumper center];
    p.x += [jumper dx];
    //p.y -= [jumper dy];
    
    //If jumper is more than half way up the screen, shift blocks down
    bool scrollBricks = true;
     if(p.y >= ((bounds.size.height)*.5)){
         p.y -= [jumper dy];
         scrollBricks = false;
        //[self shiftBlocksDown];
     }
     else if([jumper dy] <= 0){
         p.y -= [jumper dy];
         scrollBricks = false;
     }
    
    
    // If the jumper has fallen below the bottom of the screen,
    // add a positive velocity to move him
    // Also set high score here
    if (p.y > bounds.size.height)
    {
        [jumper setDy:10];
        p.y = bounds.size.height;
    }
    
    //If jumper is above the middle of the screen, scroll blocks down
    if(true == scrollBricks){
        [self shiftBlocksDown];
    }
    
    // If we've gone past the top of the screen, wrap around
    /*
    if (p.y < 0){
        p.y += bounds.size.height;
    }
     */
    
    // If we have gone too far left, or too far right, wrap around
    if (p.x < 0)
        p.x += bounds.size.width;
    if (p.x > bounds.size.width)
        p.x -= bounds.size.width;
    
    // If we are moving down, and we touch a brick, we get
    // a jump to push us up.
    if ([jumper dy] < 0)
    {
        CGRect b;
        for(IceBlock *ib in iceBlockArr){
            b = [ib frame];
            if(CGRectContainsPoint(b, p)){
                [jumper setDy:10];
                break;
            }
        }
        
        for(LavaBlock *lb in lavaBlockArr){
            b = [lb frame];
            if(CGRectContainsPoint(b, p)){
                [jumper setDy:15];
                break;
            }
        }
        
        for(CrackedBlock *cb in crackedBlockArr){
            b = [cb frame];
            if(CGRectContainsPoint(b, p)){
                [jumper setDy:10];
                [cb removeFromSuperview];
        
                if(1 == [crackedBlockArr count]){
                    [self makeCrackedBlocks:cb];
                }
                 [crackedBlockArr removeObject:cb];
                break;
            }
        }
        
        /*
        for (Brick *brick in bricks)
        {
            CGRect b = [brick frame];
            if (CGRectContainsPoint(b, p))
            {
                // Yay!  Bounce!
                NSLog(@"Bounce!");
                [jumper setDy:10];
            }
        }
         */
    }
    
    [jumper setCenter:p];
    // NSLog(@"Timestamp %f", ts);
}

-(void)makeCrackedBlocks:(CrackedBlock *) cb{
    CrackedBlock *tmp;
    CGRect bounds = [self bounds];
    int randX;
    int randY;
    int xSpacing = 0;
    for(int i = 0; i < 4; i++){
        
        randX = arc4random_uniform((int)(bounds.size.width*.8))+xSpacing;
        randY = arc4random_uniform((int)(bounds.size.height*.1))+4;
        
        tmp = [[CrackedBlock alloc] initWithFrame:CGRectMake(0, 0, cb.frame.size.width,
                                                             cb.frame.size.height)];
        [tmp setImage:[UIImage imageNamed:@"crackedblock.png"]];
        [tmp setCenter:CGPointMake(randX, randY)];
            
        [crackedBlockArr addObject:tmp];
        [self addSubview:tmp];
        xSpacing += 4;
    }
}

@end
